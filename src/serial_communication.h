#pragma once
#include <iostream>
#include <string>
#include <sstream>

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>

#include "communication.h"

//! Serial communication class provides an interface to talk to the SSC32 controller over serial USB
class Serial_communication : public Communication
{
public:
	// using Communication::buffer;
	/**
	 * Constructor. takes a baud-rate and a hardware port
	 *
	 */
Serial_communication(unsigned short a_baudrate, std::string a_port);

/**
 * set_baudrate
 * @param a_baudrate - baud rate to set
 */
void set_baudrate(unsigned short a_baudrate);

/**
 * send_message
 * @return - returns the current baud rate
 */
unsigned short get_baudrate();

/**
 * write
 * @param message: write this string to serial bus
 */
void write();



private:
 unsigned short baudrate; /// Baudrate that the communicator will use to write to serial bus
 std::string port; /// hardware port with the serial device
 boost::asio::io_service io; /// io service
 boost::asio::serial_port serial; /// boost serial port
};