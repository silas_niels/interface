#pragma once
#include <iostream>
#include <vector>
#include <memory>

#include "serial_communication.h"

class Action_handler;

//! Virtual action class.
class Action
{
public:
	/**	
	 * Action constructor
	 * @param a_handler - Every action belongs to a handler
	 */
	Action(Action_handler& a_handler);

	/**	
	 * init - overrideable function that provides initialisation capacity to subclasses
	 */
	virtual void init() = 0;

protected:
	Action_handler& ah; // Action handler that invokes the action
};



class Servo_action: public Action
{
public:
	/**	
	 * Action constructor
	 * @param a_servo_id - Servo id that the action relates to
	 * @param a_target_angle - angle to apply to the servo when the action is executed
	 * @param a_servo_speed - speed with which the servo shall achieve given angle
	 * @param a_time - Time limit in which the servo must achieve given angle
	 * @param a_handler - Every action belongs to a handler
	 */
Servo_action(unsigned short a_servo_id,signed short a_target_angle,
unsigned short a_servo_speed,unsigned short a_time, Action_handler& a_handler);

virtual ~Servo_action();

/**
 * init - applies the action to the corresponding servo
 */
void init() override;

//attributes

signed short target_angle;
unsigned short servo_speed; // in microseconds per second
unsigned short time;
unsigned short servo_id; 
};

class Robot_action: public Action
{
public:
	/**	
	 * Action constructor
	 * @param a_servo_action_vector - vector containing the servo_actions to be executed during init()
	 * @param a_handler - Every action belongs to a handler
	 */
	Robot_action(std::vector<Servo_action> a_servo_action_vector, Action_handler& a_handler);

	virtual ~Robot_action();
	/**
	 * init - the robotaction applies all its servo_actions to the corresponding servos
	 */
	void init() override;
private:
	std::vector<Servo_action> servo_action_vector; // vector containing servo_actions to be executed during init()
};
