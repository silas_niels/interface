#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "communication.h"
#include "actions.h"

class Servo_action;
class Robot_action;
class Servo;
typedef std::shared_ptr<Servo_action> Servo_action_ptr;

class Servo 
{
public:
	/**
	 * Servo constructor
	 * @param a_name - Servo name
	 * @param a_comm, - Serial communicator
	 * @param a_servo_channel - actual serial channel the servo is on
	 * @param a_min_angle - minimum angle the servo can achieve
	 * @param a_max_angle - minimum angle the servo can achieve
	 * @param a_min_pw - pulsewidth corresponding to minimum angle 
	 * @param a_max_pw - pulsewidth corresponding to maximum angle
	 * @param a_current_pw - current pulsewidth (set this to resting angle)
	 * @param a_target_pw - current target pw
     * @param a_pw_per_tick, - pw per tick
     * @param a_current_action - current action
     * @param an_amount_of_ticks - amount of ticks
	 */
Servo(std::string a_name,
	Communication& a_comm, 
	unsigned short a_servo_channel,
	signed short a_min_angle,signed short a_max_angle,
	signed short a_min_pw, signed short a_max_pw,
	signed short a_current_pw, signed short a_target_pw,
    signed short a_pw_per_tick, Servo_action_ptr a_current_action);
/**
 * check_if_valid_angle
 * @param an_angle - angle to check
 * @return - wether or not the angle falls within the servo's range
 */
bool check_if_valid_angle(signed short an_angle);
/**
 * check_if_valid_pw
 * @param a_pw - pulsewidth to check
 * @return - whether or not the pulsewidth falls within the servo's range
 */
bool check_if_valid_pw(signed short a_pw);
/**
 * check_is_idle
 * @return - is the servo currently busy?
 */
bool check_is_idle();
/**
 * get_channel
 * @return - the servo's serial channel
 */
unsigned short get_channel();
/**
 * handle - performs the servo's logic
 */
void handle();
/**
 * set_action
 * @param a_servo_action - sets the current action for the servo and initialises its new task
 */
void set_action(Servo_action_ptr a_servo_action);
/**
 * stop - stops the servo from performing another tick
 */
void stop();
/**
 * get_current_state
 * @return - returns name and current angle of the servo
 */
std::string get_current_state();

std::string getName() const;
private:
Communication& comm; // Serial communicator
double tickrate = 0.05; // time in ms between ticks... /dirtycode
bool is_idle; // working status of the servo
Servo_action_ptr current_action; // Current action the servo is working on
signed short pw_per_tick; // Pulsewidth the servo will iterate per tick to achieve target_pw
signed short current_pw; // The pulsewidth the servo is on at the moment
signed short target_pw; // Target pulsewidth the servo is currently aiming to achieve
signed short get_angle_from_pw(signed short a_pw); // Converts pw to angle
signed short get_pw_from_angle(signed short an_angle); // Converts angle to pw


// Const vars
const std::string name; // Servo's name
const unsigned short servo_channel; // Serial channel the servo is on
const signed short min_angle; // Minimum angle the servo can achieve
const signed short max_angle; // Maximum angle the servo can achieve
const signed short min_pw; // Minimum pulsewidth the servo can achieve
const signed short max_pw; // Maximum pulsewidth the servo can achieve
};
