#include "serial_communication.h"


Serial_communication::Serial_communication(unsigned short a_baudrate, std::string a_port) : port(a_port), baudrate(a_baudrate)
														,io(), serial(io,port)
{
	serial.set_option(boost::asio::serial_port_base::baud_rate(baudrate));	
}

void Serial_communication::set_baudrate(unsigned short a_baudrate)
{
  baudrate = a_baudrate;
}

unsigned short Serial_communication::get_baudrate()
{
	return baudrate;
}


void Serial_communication::write()
{
	command_buffer << std::string("\r\n");
	boost::asio::write(serial,boost::asio::buffer(command_buffer.str().c_str(),command_buffer.str().size()));
	std::cout << command_buffer.str().c_str() << std::endl;
	command_buffer.str(std::string());

}
