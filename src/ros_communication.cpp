#include "ros_communication.h"


Ros_communication::Ros_communication()
{
	pub = nh_.advertise<std_msgs::String>("SSC32_messages", 1000);
}

void Ros_communication::write()
{
    std_msgs::String msg;
    msg.data = command_buffer.str();
   	pub.publish(msg);
}