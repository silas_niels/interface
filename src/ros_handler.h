#pragma once

#include <ros/ros.h>
#include <vector>
#include <actionlib/server/simple_action_server.h>
#include <std_msgs/Int16.h>
#include <std_msgs/UInt16.h>

#include <AL5D_interface/Predefined_positionAction.h>
#include <AL5D_interface/Robotarm_positionAction.h>
#include <AL5D_interface/kill_switch.h>
#include <AL5D_interface/off_switch.h>
#include <AL5D_interface/empty_queue.h>
#include <AL5D_interface/get_robot_state.h>

#include "action_handler.h"

class Ros_handler
{
public:	
	Ros_handler(std::string name_of_predefined, std::string name_of_robotarm);
	/**	
	 * Ros handler constructor
	 * @param name_of_predefined - name of predefined action
	 * @param name_of_robotarm - name of the robot arm
	 * @param a_baudrate - baud rate for serial communication with the robot
	 * @param a_port - hardware port for serial commuincation
	 */
	Ros_handler(std::string name_of_predefined, std::string name_of_robotarm,const unsigned long a_baudrate, const std::string& a_port);
	/**
	 * ~Ros_handler - destructor
	 */
	~Ros_handler(void);
	/**
	 * tick - performs action_Handler.tick
	 */
	void tick();
	/**
	 * execute_predefined_CB - ros action handler for predefined position action
	 */
	void execute_predefined_CB(const AL5D_interface::Predefined_positionGoalConstPtr &goal);
	/**
	 * execute_robotarm_CB - ros action handler for robotarm position action
	 */
	void execute_robotarm_CB(const AL5D_interface::Robotarm_positionGoalConstPtr &goal);
	/**
	 * get_robot_state - service that returns the robot's current state
	 */
	bool get_robot_state(AL5D_interface::get_robot_state::Request &req,
           AL5D_interface::get_robot_state::Response &res);
protected:
    /**
     * kill_switch - service that kills the robot
     */
	bool kill_switch(AL5D_interface::kill_switch::Request &req,
           AL5D_interface::kill_switch::Response &res);
     /**
      * off_switch - service that places the robot into park mode
      */
	bool off_switch(AL5D_interface::off_switch::Request &req,
           AL5D_interface::off_switch::Response &res);
     /**
      * empty_queue - service that empties the action queue
      */
	bool empty_queue(AL5D_interface::empty_queue::Request &req,
           AL5D_interface::empty_queue::Response &res);

	bool can_queue;

// Ros's nodehandler
ros::NodeHandle nh_;
Action_handler ah;
// the action servers
actionlib::SimpleActionServer<AL5D_interface::Robotarm_positionAction> robotarm_as_; 
actionlib::SimpleActionServer<AL5D_interface::Predefined_positionAction> predefined_as_; 

//action names
std::string predefined_action_name_;
std::string robotarm_action_name_;

//messages for predefined
AL5D_interface::Predefined_positionFeedback predefined_feedback_;
AL5D_interface::Predefined_positionResult predefined_result_;

//mesages for robotarm
AL5D_interface::Robotarm_positionFeedback robotarm_feedback_;
AL5D_interface::Robotarm_positionResult robotarm_result_;

//services
ros::ServiceServer kill_switch_srv;
ros::ServiceServer off_switch_srv;
ros::ServiceServer empty_queue_srv;
ros::ServiceServer get_robot_state_srv;
};

