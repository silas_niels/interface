
#include "action_handler.h"
#include <unistd.h>
Action_handler::Action_handler(const unsigned long a_baudrate, const std::string& a_port) : comm(*(new Serial_communication(a_baudrate,a_port)))
{

	servos.push_back(Servo("Base",       comm, 0, -90, 90, 620,2400, 1500
		,0,0,nullptr));
	servos.push_back(Servo("Shoulder",   comm, 1, -30, 90, 1700,700, 1700
		,0,0,nullptr));
	servos.push_back(Servo("Elbow",      comm, 2, 0 ,135 ,650 ,1880, 1800
		,0,0,nullptr));
	servos.push_back(Servo("Wrist",      comm, 3, -90, 90, 2400,500, 800
		,0,0,nullptr));
	servos.push_back(Servo("WristRotate",comm, 5, -90, 90, 2500,660, 2100
		,0,0,nullptr));
	servos.push_back(Servo("Gripper",    comm, 4, -90, 90, 800,2500, 800
		,0,0,nullptr));




	predefined_positions.insert(std::make_pair("READY", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0 ,2,2000, *this),
									Servo_action(1,-30,2,2000, *this),
									Servo_action(2,110,2,2000, *this),
									Servo_action(3,0 ,2,2000, *this),
									Servo_action(4,0 ,2,2000, *this),
								    Servo_action(5,0 ,2,2000, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("INIT", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,2000, *this),
									Servo_action(1,90,1,2000, *this),
									Servo_action(2,135,1,2000, *this),
									Servo_action(3,-20,1,2000, *this),
									Servo_action(4,40,1,2000, *this),
									Servo_action(5,40,1,2000, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("STRAIGHT_UP", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,  2300, *this),
									Servo_action(1,0,1,  2300, *this),
									Servo_action(2,0,1,  2300, *this),
									Servo_action(3,0,1,  2300, *this),
									Servo_action(4,180,1,  2300, *this),
									Servo_action(5,0,1,  2300, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("PARK", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,  2300, *this),
									Servo_action(1,0,1,  2300, *this),
									Servo_action(2,110,1,  2300, *this),
									Servo_action(3,65,1,  2300, *this),
									Servo_action(4,0,1,  2300, *this),
									Servo_action(5,0,1,  2300, *this)
								   }), *this)));

  queue_preprogrammed_position("PARK");
  queue_preprogrammed_position("READY");
}

Action_handler::Action_handler(): comm(*(new Ros_communication()))
{

	servos.push_back(Servo("Base",       comm, 0, -90, 90, 610,2320, 1500
			,0,0,nullptr));
	servos.push_back(Servo("Shoulder",   comm, 1, -30, 90, 835,2200, 1700
		,0,0,nullptr));
	servos.push_back(Servo("Elbow",      comm, 2, 0 ,135 ,659,2100, 1800
		,0,0,nullptr));
	servos.push_back(Servo("Wrist",      comm, 3, -90, 90, 570,2465, 800
		,0,0,nullptr));
	servos.push_back(Servo("WristRotate",comm, 4, 0, 360, 1000,2100, 2100
		,0,0,nullptr));
	servos.push_back(Servo("Gripper",    comm, 5, -90, 90, 570,2430, 570
		,0,0,nullptr));
	predefined_positions.insert(std::make_pair("READY", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0 ,2,2000, *this),
									Servo_action(1,-30,2,2000, *this),
									Servo_action(2,110,2,2000, *this),
									Servo_action(3,0 ,2,2000, *this),
									Servo_action(4,0 ,2,2000, *this),
								    Servo_action(5,0 ,2,2000, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("INIT", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,2000, *this),
									Servo_action(1,90,1,2000, *this),
									Servo_action(2,135,1,2000, *this),
									Servo_action(3,-20,1,2000, *this),
									Servo_action(4,40,1,2000, *this),
									Servo_action(5,40,1,2000, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("STRAIGHT_UP", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,  2300, *this),
									Servo_action(1,0,1,  2300, *this),
									Servo_action(2,0,1,  2300, *this),
									Servo_action(3,0,1,  2300, *this),
									Servo_action(4,180,1,  2300, *this),
									Servo_action(5,0,1,  2300, *this)
								   }), *this)));
	predefined_positions.insert(std::make_pair("PARK", Robot_action(std::vector<Servo_action>({
									Servo_action(0,0,1,  2300, *this),
									Servo_action(1,-30,1,  2300, *this),
									Servo_action(2,110,1,  2300, *this),
									Servo_action(3,65,1,  2300, *this),
									Servo_action(4,0,1,  2300, *this),
									Servo_action(5,0,1,  2300, *this)
								   }), *this)));

  queue_preprogrammed_position("PARK");
  queue_preprogrammed_position("READY");
}

void Action_handler::tick()
{
	bool ready_for_next_action = true;
	for(Servo& s : servos)
	{
		if(!s.check_is_idle())
		{
	   		s.handle();
		}
	   	if (ready_for_next_action)
	   	{
			ready_for_next_action = s.check_is_idle();
		}
	}
	if(!comm.isEmpty())
	comm.write();
	if(ready_for_next_action && !action_queue.empty())
	{
		Action_ptr a = action_queue.front();
		a->init();
		action_queue.pop();
	}
}

void Action_handler::set_action_on_servo(unsigned short a_servo_id,Servo_action_ptr a_servo_action)
{
	servos.at(a_servo_id).set_action(a_servo_action);
}

bool Action_handler::queue_preprogrammed_position(const std::string& a_name)
{
	std::map<std::string,Robot_action>::iterator p_p_it = predefined_positions.find(a_name);
	if( p_p_it != predefined_positions.end())
	{
		Robot_action& ra = predefined_positions.at(a_name);
		action_queue.push(std::make_shared<Robot_action>(ra));
		std::cout << action_queue.size() << std::endl;
		std::cout << " adding to queue" << std::endl;
		return true;
	}
	else
	{
		std::string error = "ERROR " + a_name + " doenst exist";
		ROS_ERROR(error.c_str());
		return false;
	}
}

bool Action_handler::empty_queue()
{
  	std::queue<Action_ptr>().swap(action_queue);
  	return true;
}

bool Action_handler::kill_switch()
{
	std::cout << "Killin'\n";
	empty_queue();
	for (Servo& s : servos)
	{
		s.stop();
	}
	return true;
}

bool Action_handler::off_switch()
{
	empty_queue();
	queue_preprogrammed_position("PARK");
	return true;
}

bool Action_handler::queue_robot_position(std::vector<AL5D_interface::Servo_position_action> spa_vec)
{
	std::vector<Servo_action> sa_vec;

	for (AL5D_interface::Servo_position_action& spa : spa_vec)
	{
		if(!servos.at(spa.servo_id).check_if_valid_angle(spa.angle))
		{
			std::string error = "Servo "+ servos.at(spa.servo_id).getName() + ": angle " + std::to_string(spa.angle) + " is invalid";
			ROS_ERROR(error.c_str());
			return false;
		}
		sa_vec.push_back(Servo_action(spa.servo_id, spa.angle, spa.speed, spa.time, *this));
	}
	//std::cout << "added queue_robot_position" << std::endl;
	action_queue.push(std::make_shared<Robot_action>(Robot_action(sa_vec, *this)));
	return true;
	//std::cout << action_queue.size() << std::endl;

}

std::string Action_handler::get_robot_state()
{
	std::string return_string;
	return_string = "amount of actions in queue: " + std::to_string(action_queue.size()) + "\n"; 
	for(Servo& s: servos) 
	{
		return_string += s.get_current_state();
	}

	return return_string;
}
