#include "ros_handler.h"
#include <iostream>
#include <memory>

std::shared_ptr<Ros_handler> create_ros_handler(int argc, char** argv)
{
	if(argc == 3)
	{
		ROS_INFO("started with given port and baudrate on serial");
		std::cout<< "baudrate: " << argv[1] << "port: " << argv[2] << std::endl;
		return std::make_unique<Ros_handler>("predefined" ,"robotarm", std::stoi(argv[1]),argv[2]);
	}
	else if( argc > 3)
	{
		ROS_ERROR("INVALID AMOUNT OF ARGUMENTS, started simulating");
		return std::make_unique<Ros_handler>("predefined" ,"robotarm");
		
	}
	else
	{
		ROS_INFO("started simulator");
		return std::make_unique<Ros_handler>("predefined" ,"robotarm");
	}
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "Server");

    std::shared_ptr<Ros_handler> ros_handler = create_ros_handler(argc, argv);
    ros::Rate r(20);
	while(ros::ok())
	{
		ros_handler->tick();
		ros::spinOnce();
		r.sleep();
	}


	return 0;
}
