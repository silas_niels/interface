#pragma once
#include <iostream>
#include <string>

#include <std_msgs/String.h> 
#include <ros/ros.h>

#include "communication.h"

// topic: "SSC32_messages"

//! Serial communication class provides an interface to talk to the SSC32 controller over serial USB
class Ros_communication : public Communication
{
public:
	/**
	 * Constructor. takes a baud-rate and a hardware port
	 *
	 */
Ros_communication();

/**
 * write
 * @param message: write this string to serial bus
 */
void write();

private:
ros::NodeHandle nh_;
ros::Publisher pub;
};