#include <ros/ros.h>
#include <AL5D_interface/Predefined_positionAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<AL5D_interface::Predefined_positionAction> PredefinedClient;

int main(int argc, char** argv){
  if(argc < 2)
  {
    return 1;
  }
  ros::init(argc, argv, "predefined_client");

  //tell the action client that we want to spin a thread by default
  PredefinedClient pc("predefined", true);

  //wait for the action server to come up
  while(!pc.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the server to come up");
  }

  AL5D_interface::Predefined_positionGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.name_of_position = argv[1];
  ROS_INFO("Sending goal");
  pc.sendGoal(goal);

  pc.waitForResult();

  if(pc.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Hooray, the base moved 1 meter forward");
  else
    ROS_INFO("The base failed to move forward 1 meter for some reason");

  return 0;
}
