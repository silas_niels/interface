#pragma once
#include <iostream>
#include <sstream>
#include <string>

//! Serial communication class provides an interface to talk to the SSC32 controller over serial USB
class Communication
{
public:
	/**
	 * Constructor. takes a baud-rate and a hardware port
	 *
	 */
Communication()
{}
/**
 * send_message
 * @param message: string to write to serial bus.
 */
void send_message(std::string message);

/**
 * write
 * @param message: write this string to serial bus
 */
virtual void write() = 0;

/**
 * write overload to write servo information to the bus directly
 * @param an_id - servo id
 * @param a_pw - pulsewidth to write to servo
 * @param a_speed - speed to write to servo
 * @param a_time - time to write to servo
 */
void buffer_command(unsigned short an_id, unsigned short a_pw, unsigned short a_speed, unsigned short a_time);
bool isEmpty();

protected:

std::ostringstream command_buffer;

};
