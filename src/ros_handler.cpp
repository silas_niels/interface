#include "ros_handler.h"

Ros_handler::Ros_handler(std::string name_of_predefined, std::string name_of_robotarm, const unsigned long a_baudrate, const std::string& a_port ):
    robotarm_as_(nh_, name_of_robotarm, boost::bind(&Ros_handler::execute_robotarm_CB, this, _1),false),
    predefined_as_(nh_, name_of_predefined, boost::bind(&Ros_handler::execute_predefined_CB, this, _1),false),
    predefined_action_name_(name_of_predefined),
    robotarm_action_name_(name_of_robotarm),
    ah(a_baudrate, a_port), can_queue(true)
{
  robotarm_as_.start();
  predefined_as_.start();
  kill_switch_srv = nh_.advertiseService("kill_switch", &Ros_handler::kill_switch, this);
  off_switch_srv = nh_.advertiseService("off_switch", &Ros_handler::off_switch, this);
  empty_queue_srv = nh_.advertiseService("empty_queue", &Ros_handler::empty_queue, this);
  get_robot_state_srv = nh_.advertiseService("get_robot_state", &Ros_handler::get_robot_state, this);
}

Ros_handler::Ros_handler(std::string name_of_predefined,
		std::string name_of_robotarm):
robotarm_as_(nh_, name_of_robotarm, boost::bind(&Ros_handler::execute_robotarm_CB, this, _1),false),
	predefined_as_(nh_, name_of_predefined, boost::bind(&Ros_handler::execute_predefined_CB, this, _1),false),
	predefined_action_name_(name_of_predefined),
	robotarm_action_name_(name_of_robotarm),
	ah(), can_queue(true)
{
  robotarm_as_.start();
  predefined_as_.start();
  kill_switch_srv = nh_.advertiseService("kill_switch", &Ros_handler::kill_switch, this);
  off_switch_srv = nh_.advertiseService("off_switch", &Ros_handler::off_switch, this);
  empty_queue_srv = nh_.advertiseService("empty_queue", &Ros_handler::empty_queue, this);
  get_robot_state_srv = nh_.advertiseService("get_robot_state", &Ros_handler::get_robot_state, this);
}

Ros_handler::~Ros_handler(void)
{

}
void Ros_handler::tick()
{
  ah.tick();
}
void Ros_handler::execute_predefined_CB(const AL5D_interface::Predefined_positionGoalConstPtr &goal)
{
   ROS_INFO("executing predefined");
   bool success = false;
 	//add actions
   if(can_queue)
   {
  	success = ah.queue_preprogrammed_position(goal->name_of_position);
   }
   //execute single step
	if(success)
	{
	 //result_.sequence = feedback_.sequence;
	 ROS_INFO("%s: Succeeded", predefined_action_name_.c_str());
	// set the action state to succeeded
	 predefined_as_.setSucceeded(predefined_result_);
	}
	else
	{
	 //result_.sequence = feedback_.sequence;
	 ROS_INFO("%s: ABORT", robotarm_action_name_.c_str());
	// set the action state to succeeded
	 robotarm_as_.setAborted(robotarm_result_);
	}
}

void Ros_handler::execute_robotarm_CB(const AL5D_interface::Robotarm_positionGoalConstPtr &goal)
{
   ROS_INFO("executing robotarm");

   bool success = false;
    std::vector<AL5D_interface::Servo_position_action> list_of_actions = goal->target_position;
   	if(can_queue)
   	{
    	success = ah.queue_robot_position(list_of_actions);
	}
   //execute single step
	if(success)
	{
	 //result_.sequence = feedback_.sequence;
	 ROS_INFO("%s: Succeeded", robotarm_action_name_.c_str());
	// set the action state to succeeded
	 robotarm_as_.setSucceeded(robotarm_result_);
	}
	else
	{
	 //result_.sequence = feedback_.sequence;
	 ROS_INFO("%s: ABORTED", robotarm_action_name_.c_str());
	// set the action state to succeeded
	 robotarm_as_.setAborted(robotarm_result_);
	}
}


bool Ros_handler::kill_switch(AL5D_interface::kill_switch::Request  &req,
         AL5D_interface::kill_switch::Response &res)
{
	if (req.off)
	{
		can_queue = false;
  		ah.kill_switch();  
	}
	else
	{
		can_queue = true;
	}
	res.result = can_queue;
	return true;
}
bool Ros_handler::off_switch(AL5D_interface::off_switch::Request &req,
           AL5D_interface::off_switch::Response &res) 
{
   can_queue = false;
   ah.off_switch();
   return true;
}
bool Ros_handler::empty_queue(AL5D_interface::empty_queue::Request &req,
           AL5D_interface::empty_queue::Response &res)
{
   ah.empty_queue();
   return true;
}

bool Ros_handler::get_robot_state(AL5D_interface::get_robot_state::Request &req,
    AL5D_interface::get_robot_state::Response &res)
{
	// Fill this with the result
	 res.state_string = ah.get_robot_state();
	 return true;
}

