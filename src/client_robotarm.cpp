#include <ros/ros.h>
#include <AL5D_interface/Robotarm_positionAction.h>
#include <actionlib/client/simple_action_client.h>
#include <vector>
#include <sstream>
#include <iostream>
typedef actionlib::SimpleActionClient<AL5D_interface::Robotarm_positionAction> RobotArmClient;

int main(int argc, char** argv){
  if(argc  < 2)
  {
  	ROS_ERROR(" not enough arguments");
    return 1;
  }
  ros::init(argc, argv, "robotarm_client");

  //tell the action client that we want to spin a thread by default
  RobotArmClient pc("robotarm" , true);

  //wait for the action server to come up
  while(!pc.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the server to come up");
  }

  AL5D_interface::Robotarm_positionGoal goal;
  std::vector<AL5D_interface::Servo_position_action> servo_action_list;

  for(int i =1; i < argc; i ++)
  {

  	AL5D_interface::Servo_position_action spa;
  	std::istringstream ss(argv[i]);
	  std::string token;
	  std::getline(ss, token, ',');
	  spa.servo_id = std::stoi(token);
	  std::getline(ss, token, ',');
	  spa.angle = std::stoi(token);
	  std::getline(ss, token, ',');
	  spa.time = std::stoi(token);
	  servo_action_list.push_back(spa);

  }
 
  goal.target_position = servo_action_list;
  ROS_INFO("Sending goal");
  pc.sendGoal(goal);

  pc.waitForResult();

  if(pc.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Hooray, the base moved");
  else
    ROS_INFO("The base failed to move for some reason");
  return 0;
}
