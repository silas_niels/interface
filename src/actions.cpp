#include "actions.h"
#include "action_handler.h"

Action::Action(Action_handler& a_handler) : ah(a_handler)
{
}

Servo_action::Servo_action(unsigned short a_servo_id,signed short a_target_angle,
unsigned short a_servo_speed,unsigned short a_time, Action_handler& a_handler)
:target_angle(a_target_angle), servo_speed(a_servo_speed),time(a_time),servo_id(a_servo_id), Action(a_handler)
{

}

void Servo_action::init()
{
	 ah.set_action_on_servo(servo_id,std::make_shared<Servo_action>(*this));
}

Robot_action::Robot_action(std::vector<Servo_action> a_servo_action_vector, Action_handler& a_handler)
:servo_action_vector(a_servo_action_vector), Action(a_handler)
{

}

void Robot_action::init()	
{
	for(Servo_action& sa : servo_action_vector)
	{
		sa.init();
	}
}

Servo_action::~Servo_action() {
}

Robot_action::~Robot_action() {
}
