#include "communication.h"

void Communication::send_message(std::string message)
{
	//std::cout << message << std::endl;
}

void Communication::buffer_command(unsigned short an_id, unsigned short a_pw, unsigned short a_speed, unsigned short a_time)
{
	command_buffer << (std::string("#") + std::to_string(an_id) +
	                            std::string("P") + std::to_string(a_pw)
	                            // std::string("S") + std::to_string(a_speed) +
	                            // std::string("T") + std::to_string(a_time) +
	                            );
}

bool Communication::isEmpty()
{
	return command_buffer.str().size() == 0;
}
