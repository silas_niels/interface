#pragma once
#include <queue>
#include <memory> 
#include <iostream>
#include <map>
#include <vector>

#include <AL5D_interface/Robotarm_positionAction.h>

#include "servo.h"
#include "actions.h"
#include "ros_communication.h"


typedef std::shared_ptr<Action> Action_ptr;

//! action_handler that manages actions
class Action_handler 
{
public:
	Action_handler();
  /** 
   * Action handler constructor
   * @param a_baudrate - Baudrate to pass on to Serial_communication
   * @param a_port - Hardware port to pass on to Serial_communication
   */
  Action_handler(const unsigned long a_baudrate, const std::string& a_port);
  /**
   * tick - performs all the servo movement logic and action execution
   */
  void tick();
  /**
   * set_action_on_servo
   * @param a_servo_id - id to place the servo_action on
   * @param a_servo_action - servo action to place on the servo
   */
  void set_action_on_servo(unsigned short a_servo_id,Servo_action_ptr a_servo_action);
  /**
   * queue_robot_position - creates a robot position from a set of servo position actions given by the ros_handler
   * @param spa_vec - Servo position actions vector
   */
  bool queue_robot_position(std::vector<AL5D_interface::Servo_position_action> spa_vec);
  /**
   * queue_preprogrammed_position - finds a corresponding position from the predefined_position map and applies it to the queue
   * @param a_name - name of the preprogrammed position
   */
  bool queue_preprogrammed_position(const std::string& a_name);

  /**
   * get_robot_state
   * @return a collection of information pertaining to the current robot status
   */
  std::string get_robot_state();
  /** 
   * empty_queue - empties the action_queue so no further actions will be sheduled
   */
  bool empty_queue();
  /**
   * kill_switch - freezes the robot in place and kills the queue/actions
   */
  bool kill_switch();
  /**
   * off_switch - empties the queue and schedules a park action
   */
  bool off_switch();
private:
  Communication& comm; // Serial communicator
  std::vector<Servo> servos; // List of servos to control
  std::queue<Action_ptr> action_queue; // queue of actions
  std::map<std::string,Robot_action> predefined_positions; // List of predefined positions
};	
