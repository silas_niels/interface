#include <cmath>
#include "servo.h"

Servo::Servo(std::string a_name, 
	Communication& a_comm,
	unsigned short a_servo_channel,
	signed short a_min_angle,
	signed short a_max_angle,
	signed short a_min_pw, 
	signed short a_max_pw,
    signed short a_current_pw, 
    signed short a_target_pw,  
    signed short a_pw_per_tick, 
    Servo_action_ptr a_current_action)
			   :name(a_name),
			    comm(a_comm),
				servo_channel(a_servo_channel),
 				min_angle(a_min_angle),max_angle(a_max_angle),
 				min_pw(a_min_pw),max_pw(a_max_pw),
				current_pw(a_current_pw),target_pw(a_target_pw),
				pw_per_tick(a_pw_per_tick),current_action(a_current_action),
				is_idle(true)
{

}

bool Servo::check_if_valid_angle(signed short an_angle)
{
	return an_angle >= min_angle && an_angle <= max_angle;
}
bool Servo::check_if_valid_pw(signed short a_pw)	
{
	return a_pw >= min_pw && a_pw <= max_pw;
}
unsigned short Servo::get_channel()
{
	return servo_channel;
}

bool Servo::check_is_idle()
{
	return is_idle;
}

void Servo::handle()
{
	//if (servo_channel == 0 ) std::cout << std::abs(target_pw-current_pw) << " " << pw_per_tick << std::endl;
	if (!is_idle && std::abs(target_pw-current_pw) >= std::abs(pw_per_tick) && current_pw != target_pw)
	{		
		current_pw += pw_per_tick;
		if (std::abs(current_pw-target_pw)<std::abs(pw_per_tick)) current_pw = target_pw;
		comm.buffer_command(servo_channel, current_pw, 100, 100);
	}
	else
	{
		// std::cout << get_angle_from_pw(current_pw) << std::endl;
		is_idle = true;
	}
}
void Servo::set_action(Servo_action_ptr a_servo_action)
{
	current_action = a_servo_action;
	is_idle = false;

	target_pw = get_pw_from_angle(current_action->target_angle); // convert given angle to pwm

	double tick_count_within_time = ((double)current_action->time)/1000 / tickrate;

	double delta_pw = (target_pw - current_pw); // calculate difference in pwm
	double pw_per_tick_unrounded = delta_pw / tick_count_within_time;
	pw_per_tick = (pw_per_tick_unrounded < 0) ? floor(pw_per_tick_unrounded) : ceil(pw_per_tick_unrounded);
	std::cout << "pw_per_tick "<<  pw_per_tick << " delta_pw " << delta_pw << std::endl;
}
signed short Servo::get_angle_from_pw(signed short a_pw)
{
	return (a_pw - min_pw) * (max_angle - min_angle) / (max_pw - min_pw) + min_angle;
}
signed short Servo::get_pw_from_angle(signed short an_angle)
{
	
	return (an_angle - min_angle) * (max_pw - min_pw) / (max_angle - min_angle) + min_pw;
}

void Servo::stop()
{
	is_idle = true;
	pw_per_tick = 0;
}

std::string Servo::get_current_state()
{
	return std::string(name + ": " + std::to_string(get_angle_from_pw(current_pw)) + " \n");
}

std::string Servo::getName() const
{
	return name;
}

