#!/bin/bash
rosrun AL5D_interface al5d_predefined_client STRAIGHT_UP
sleep 5
#verschillende verplaatsingen
rosrun AL5D_interface al5d_predefined_client PARK
rosservice call /get_robot_state
rosrun AL5D_interface al5d_predefined_client READY
rosrun AL5D_interface al5d_robotarm_client 0,90,5000 1,90,5000 2,90,5000 3,90,5 4,90,5000 5,90,5000
rosrun AL5D_interface al5d_robotarm_client 0,45,3000 1,45,3000 2,45,3000 3,45,3000 4,45,5000 5,45,5000
rosrun AL5D_interface al5d_predefined_client STRAIGHT_UP
rosrun AL5D_interface al5d_robotarm_client 0,-45,3000 4,90,5000 5,-90,5000
rosrun AL5D_interface al5d_robotarm_client 0,90,10000 1,90,10000 2,90,10000 3,90,10000 4,90,10000 5,90,10000
rosrun AL5D_interface al5d_robotarm_client 0,45,3000 1,45,3 2,45,3000 3,45,3000 4,45,5000 5,45,5000
rosrun AL5D_interface al5d_predefined_client STRAIGHT_UP
rosservice call /get_robot_state
sleep 19
echo 'calling kill_switch'
rosservice call /get_robot_state
rosservice call /kill_switch true
sleep 2
rosservice call /kill_switch false
rosservice call /off_switch
